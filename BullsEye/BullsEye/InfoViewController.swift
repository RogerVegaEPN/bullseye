//
//  InfoViewController.swift
//  BullsEye
//
//  Created by Roger on 17/11/17.
//  Copyright © 2017 Roger. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }


    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil) //pasa a la ventana que estaba atras
    }
    
}
