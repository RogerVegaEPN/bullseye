//
//  Game.swift
//  BullsEye
//
//  Created by Roger on 13/11/17.
//  Copyright © 2017 Roger. All rights reserved.
//

import Foundation

class Game {
    private var score:Int
    private var target:Int
    private var round:Int //didset cuando round cambia target cambia
    {
        didSet{
            target = Int(arc4random_uniform(99)+1)
        }
    }

    
    init() {
        score = 0
        round = 1
        target = Int(arc4random_uniform(99)+1)
    }
    func getValues() -> (score:Int, round:Int, target:Int){
        return(score, round, target)
    }
    
    
    func play(sliderValue:Int)->Int{
        print(sliderValue)
        print(target)
        var puntaje = 0
        if sliderValue == target{
            score += 100
            puntaje = 100
        } else if sliderValue >= target - 3 && sliderValue <= target + 3{
            score += 75
            puntaje = 75
        } else if sliderValue >= target - 10 && sliderValue <= target + 10{
            score += 50
            puntaje = 50
        }
        round += 1
        return puntaje
    }
    
    func reiniciar(){
        score = 0
        round = 1
    }
}
