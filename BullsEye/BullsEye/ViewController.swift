//
//  ViewController.swift
//  BullsEye
//
//  Created by Roger on 13/11/17.
//  Copyright © 2017 Roger. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var roundV: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    let game = Game()
    
    //MARK:- ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        updateValues()
    }

    //MARK:- Actions
    @IBAction func playButtonPressed(_ sender: Any) {
        let sliderValue = Int(round(slider.value))

        let points = game.play(sliderValue: sliderValue)
        
        //Alerta
        let alertController = UIAlertController(title: "Resultado", message: "El valor seleccionado es \(sliderValue), tu puntaje es \(points)", preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "Aceptar", style: .default){ (action) in self.updateValues()} //Al aceptar actualiza los valores
        alertController.addAction(acceptAction)
        present(alertController, animated: true, completion: nil)
       
    }
    
    func updateValues(){
        let (gScore, gRound, gTarget) = game.getValues()
        value.text = "\(gTarget)"
        score.text = "\(gScore)"
        roundV.text = "\(gRound)"
    }
    
    @IBAction func restartButtonPressed(_ sender: Any) {
        game.reiniciar()
        updateValues()
    }
    
}

